import express from "express";
import bodyParser from "body-parser";
import {
  getNotes,
  getNoteById,
  createNote,
  updateNote,
  deleteNote,
} from "./notes.js";

const app = express();

app.use(bodyParser.json());

app.get("/notes", getNotes);
app.get("/notes/:id", getNoteById);
app.post("/notes", createNote);
app.put("/notes/:id", updateNote);
app.delete("/notes/:id", deleteNote);

app.listen(5000, () => console.log("Server running on port 5000"));
