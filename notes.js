import { nanoid } from "nanoid";

let notes = [];

// Get all notes
export const getNotes = (req, res) => {
  try {
    res.status(200).json(notes);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Get a single note
export const getNoteById = (req, res) => {
  try {
    const note = notes.find((note) => note.id === req.params.id);
    if (note) {
      res.status(200).json(note);
    } else {
      res.status(404).json({ message: "Note not found" });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Create a note
export const createNote = (req, res) => {
  try {
    const id = nanoid(10);
    const note = { id, ...req.body };
    notes.push(note);
    res.status(201).json(note);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Update a note by id
export const updateNote = (req, res) => {
  try {
    const note = notes.find((note) => note.id === req.params.id);
    if (note) {
      const index = notes.indexOf(note);
      notes[index] = { ...note, ...req.body };
      res.status(200).json(notes[index]);
    } else {
      res.status(404).json({ message: "Note not found" });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Delte a note by id
export const deleteNote = (req, res) => {
  try {
    const note = notes.find((note) => note.id === req.params.id);
    if (note) {
      notes = notes.filter((note) => note.id !== req.params.id);
      res.status(200).json({ message: "Note deleted" });
    } else {
      res.status(404).json({ message: "Note not found" });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
